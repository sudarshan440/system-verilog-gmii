// Code your testbench here
// or browse Examples
`include "GMII_intf.sv" 
`include "GMIIPkg.sv" 
module top;
  import GMIIPkg::*;
  
  logic [7:0] txd;
  logic txen;
  logic txc;
  logic txer;
  
  assign txd = gmii_intf.txd;
  assign txen = gmii_intf.txen;
  assign txer = gmii_intf.txer;
  assign txc = gmii_intf.txc;
  
  
  // Components
  PacketGen   packetGen;
  GMIIDriver  gmiiDriver;
  GMIIMonitor gmiiMonitor_a;
  GMIIMonitor gmiiMonitor_e;
  GMIIChecker gmiiChecker;
  
  // Mail boxes
  mailbox#(Packet) p2dmbox;
  mailbox#(Packet) m2cmbox_a;
  mailbox#(Packet) m2cmbox_e;
  
  // Actual interface instance
  GMII_intf gmii_intf();
  
  initial begin
    
    // component creation
    packetGen = new();
    gmiiDriver = new(gmii_intf);
    gmiiMonitor_a = new(gmii_intf);
    gmiiMonitor_e = new(gmii_intf);
    gmiiChecker = new();
    p2dmbox = new(1);
    m2cmbox_a = new(1);
    m2cmbox_e = new(1);
    
    
    // connections
    packetGen.p2dmbox = p2dmbox;
    gmiiDriver.p2dmbox = p2dmbox;
    
    gmiiMonitor_a.m2cmbox = m2cmbox_a;
    gmiiMonitor_e.m2cmbox = m2cmbox_e;
    
    gmiiChecker.m2cmbox_a = m2cmbox_a;
    gmiiChecker.m2cmbox_e = m2cmbox_e;
    
    // running the test bench
    fork
      packetGen.generatePkts();
      gmiiDriver.drive();
      gmiiMonitor_a.monitor();
      gmiiMonitor_e.monitor();
      gmiiChecker.compare();
    join
    
  end
  
  initial begin
    gmii_intf.txc = 0;
    forever begin
      #4ns gmii_intf.txc = !gmii_intf.txc;
    end
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,top);
    #25000ns;
    $finish();
  end
  
  
endmodule : top