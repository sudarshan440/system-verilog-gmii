class Packet;
  rand bit[7:0] preamble;
  rand bit[7:0] srcAddress[6] ;
  rand bit[7:0] destAddress[6];
  rand bit[7:0] data[];
  rand bit[7:0] crc[4];
  
  constraint data_size_c{ data.size == 100; }
  
  task calculateCrc();
    crc[0] = 0;
    crc[1] = 1;
    crc[2] = 2;
    crc[3] = 3;
  endtask : calculateCrc
  
endclass : Packet