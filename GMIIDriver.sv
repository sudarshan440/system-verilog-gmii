class GMIIDriver;
  mailbox#(Packet) p2dmbox;
  Packet packet;
  virtual GMII_intf vintf;
  
  function new(virtual GMII_intf vintf);
    this.vintf = vintf;
  endfunction : new
  
  task drive();
    forever begin
      p2dmbox.get(packet);
      driveInterface(packet);
    end
  endtask : drive
  
  
  task driveInterface( Packet packet );
    @(posedge vintf.txc);
    vintf.txd = packet.preamble;
    vintf.txen = 1;
    vintf.txer = 0;
    for(int i = 0 ;i < 6;i++) begin
      @(posedge vintf.txc);
      vintf.txd = packet.srcAddress[i];
    end
    for(int i = 0 ;i < 6;i++) begin
      @(posedge vintf.txc);
      vintf.txd = packet.destAddress[i];
    end
    foreach(packet.data[i]) begin
      @(posedge vintf.txc);
      vintf.txd = packet.data[i];
    end
    for(int i = 0 ;i < 4;i++) begin
      @(posedge vintf.txc);
      vintf.txd = packet.crc[i];
    end
    repeat(10) begin
      @(posedge vintf.txc);
      vintf.txen = 0;
      
    end
  endtask : driveInterface 
endclass : GMIIDriver