class GMIIMonitor;
  mailbox#(Packet) m2cmbox;
  Packet packet;
  virtual GMII_intf vintf;
  
  function new(virtual GMII_intf vintf);
    this.vintf = vintf;
  endfunction : new
  
  task monitor();
    forever begin
      monitorInterface();
    end
  endtask : monitor
  
  
  task monitorInterface( );
    bit[7:0] temp[$];
    temp = {};
    forever begin
      @(posedge vintf.txc);
      if( vintf.txen ) begin
         temp = {temp, vintf.txd};
      end else begin
        break;
      end
    end 
    if( temp.size ) begin
    packet = new();
    packet.preamble = temp.pop_front();
    for( int i = 0; i<6;i++) begin
      packet.srcAddress[i] = temp.pop_front();
    end 
    for (int i = 0; i<6;i++) begin
      packet.destAddress[i] = temp.pop_front();
    end
    for(int i = 0;i<4;i++) begin
      packet.crc[3-i] = temp.pop_back();
    end
    packet.data =new[temp.size()];
    foreach( temp[i]) begin
      packet.data[i] = temp[i];
    end
      m2cmbox.put(packet);
    end 
  endtask : monitorInterface 
endclass : GMIIMonitor