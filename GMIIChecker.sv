class GMIIChecker;
  mailbox#(Packet) m2cmbox_a;
  mailbox#(Packet) m2cmbox_e;
  
  task compare();
    Packet packet_a;
    Packet packet_e;
    bit no = 0;
    forever begin
      m2cmbox_e.get(packet_e);
      m2cmbox_a.get(packet_a);
      no =0;
      if( packet_a.preamble == packet_e.preamble &&
          packet_a.srcAddress == packet_e.srcAddress &&
         packet_a.destAddress == packet_e.destAddress &&
         packet_a.crc == packet_e.crc) begin
        foreach(packet_e.data[i]) begin
          if( packet_e.data[i] != packet_a.data[i]) begin
            no = 1;
          end
        end
        if( no ) begin
          $display("Packet MisMatch");
        end else begin
          $display("Packet Match");
        end
      end else begin
        $display("Packet Mismatch");
      end
    end
  endtask : compare
endclass : GMIIChecker