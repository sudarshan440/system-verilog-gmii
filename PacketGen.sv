class PacketGen;
  int    numOfPkts;
  Packet packet;
  mailbox#(Packet) p2dmbox;
  
  function new(int numOfPkts=10);
    this.numOfPkts = numOfPkts;
  endfunction : new
  
  task generatePkts();
    repeat(numOfPkts) begin
      packet = new();
      void'(packet.randomize());
      packet.calculateCrc();
      p2dmbox.put(packet);
    end 
  endtask : generatePkts
endclass : PacketGen